package com.common.core;

import java.util.Properties;

import com.google.gson.Gson;

public class Config {

	public enum Locale {
	    ENGLISH,
	    DUTCH,
	    FRENCH,
	    GERMAN
	  }
	
	public static final String HIKARI_DS = "HIKARI";
	public static final String DBCP_DS = "DBCP";
	
	
	public static final String APP_DS = HIKARI_DS;
	//public static final Gson GSON = new Gson();
	//public static Properties PROPS = new Properties();
}
