package com.common.core.blacklisting;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Blacklist {

	List<String> BLACKLIST;
	public abstract boolean isBlacklisted(String id);
	
	public abstract void setBlacklist(List<String> list);
}
