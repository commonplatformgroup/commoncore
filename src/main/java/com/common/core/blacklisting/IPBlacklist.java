package com.common.core.blacklisting;

import java.util.List;

public class IPBlacklist extends Blacklist {

	public boolean isBlacklisted(String ipAddress) {
		if (BLACKLIST.contains(ipAddress))
			return true;
		return false;
	}

	@Override
	public void setBlacklist(List<String> list) {
		BLACKLIST = list;
	}
}
