package com.common.core.network;

import com.common.core.errorhandling.SecException;

public interface SecNetworkClient {

	public SecHttpResponse invoke(SecHttpRequest secHttpRequest) throws SecException;
}


