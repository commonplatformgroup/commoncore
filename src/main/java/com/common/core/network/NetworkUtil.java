
package com.common.core.network;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Consts;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.common.core.logging.SecLogger;
import com.google.gson.Gson;
public class NetworkUtil {

    private static Gson GSON = new Gson();

    protected static JSONObject toJSONObject(Object obj) throws JSONException {

        SecLogger.debug("GSON toJson on " + obj.getClass());
        String json = GSON.toJson(obj);
        return new JSONObject(json);
    }

    protected static Map<String, String> populateContentTypeHeaders(SecHttpRequest req) {
        if (req.getPayloadType().equals(SecHttpRequest.PayloadType.JSON)) {
            req.getHeaders().put("Content-Type", "application/json");
            req.getHeaders().put("Accept", "application/json");
            SecLogger.debug("Adding header Content-Type: application/json");
            SecLogger.debug("Adding header Accept: application/json");
            
        } else if (req.getPayloadType().equals(SecHttpRequest.PayloadType.URLENCODED)) {
        	req.getHeaders().put("Content-Type", "application/x-www-form-urlencoded");
            //req.getHeaders().put("Accept", "application/json");
            SecLogger.debug("Adding header Content-Type: application/json");
            //SecLogger.debug("Adding header Accept: application/json");
        }else {
            req.getHeaders().put("Content-Type", "application/text");
            SecLogger.debug("Adding header Content-Type: application/text");
        }
        return req.getHeaders();
    }

    public static UrlEncodedFormEntity  createFormEntity(Map<String, String> map) {
		List<NameValuePair> form = new ArrayList<>();
		if (map == null)
			return null;
		map.forEach((key, value)-> {
			form.add(new BasicNameValuePair(key, value));
		});
        
        
        return new UrlEncodedFormEntity(form, Consts.UTF_8);
	}
    protected static String secRequestToPrintableString(SecHttpRequest request) {
        StringBuilder str = new StringBuilder("");
        str.append("PATH : ").append(request.getURL().getPath()).append("\n");
        str.append("Headers : ").append("\n");
        Map<String, String> headers = request.getHeaders();
        if (headers != null)
            headers.forEach((key, value) ->
                    str.append("\t").append(key).append(" : ").append(value).append("\n"));

        str.append("Query Params : ").append("\n");
        Map<String, String> queryParams = request.getQueryParams();
        if (queryParams != null)
            queryParams.forEach((key, value) ->
                    str.append("\t").append(key).append(" : ").append(value).append("\n"));

        try {
            if (request.getBody() != null)
                str.append("Body : ").append(NetworkUtil.toJSONObject(request.getBody()));
        } catch (JSONException e) {
            SecLogger.error(e.getMessage(), e);
        }
        return str.toString();
    }

    public static String appendQueryParamsToUrl(String url, Map<String, String> parameters) {
		URI uri = null;
		try {
			uri = new URI(url);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		String query = uri.getQuery();

		StringBuilder builder = new StringBuilder();

		if (query != null)
			builder.append(query);

		for (Map.Entry<String, String> entry : parameters.entrySet()) {
			String keyValueParam = entry.getKey() + "=" + entry.getValue();
			if (!builder.toString().isEmpty())
				builder.append("&");

			builder.append(keyValueParam);
		}

		URI newUri = null;
		try {
			newUri = new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), builder.toString(), uri.getFragment());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return newUri.toString();
	}

}
