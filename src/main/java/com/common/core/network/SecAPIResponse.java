package com.common.core.network;

import com.common.core.errorhandling.SecErrorCode;
import com.common.core.errorhandling.SecException;

public class SecAPIResponse {

	
	private Object result;
	private SecException error;
	/**
	 * @return the result
	 */
	public Object getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(Object result) {
		this.result = result;
	}
	/**
	 * @return the error
	 */
	public SecException getError() {
		return error;
	}
	/**
	 * @param error the error to set
	 * 
	 */
	public void setError(SecException error) {
		this.error = error;
	}
	
	
}
