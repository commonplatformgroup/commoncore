package com.common.core.db.cp;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;


public class DBCPDBConnectionImpl implements DBConnection{
    
    private static BasicDataSource ds = new BasicDataSource();
    
    protected DBCPDBConnectionImpl(){ }
    
    
    
    @Override
	public void createDataSource(DBDetailsModel dbDetails, Properties props) {
		// TODO Auto-generated method stub
		//config.setJdbcUrl("jdbc:h2:mem:test");
        //config.setUsername("user");
        //config.setPassword("password");
        
    	
    	
    	ds.setUrl(dbDetails.getConnectionURL());
    	ds.setUsername(dbDetails.getDbUsername());
    	ds.setPassword(dbDetails.getDbPassword());
    	
    	Properties pro = new Properties();
    	pro.getProperty("MinIdle");
    	pro.getProperty("MaxIdle");
    	pro.getProperty("MaxOpenPreparedStatements");
//    	ds.setMinIdle(5);
//        ds.setMaxIdle(10);
//        ds.setMaxOpenPreparedStatements(100);
//        
       
	}

	@Override
	public Connection getConnection() throws SQLException{
		// TODO Auto-generated method stub
		//try {
			return ds.getConnection();
		//} catch (SQLException e) {
		//	throw new SecException(SecErrorCode.SQL_EXCEPTION.getErrorCode(), SecErrorCode.SQL_EXCEPTION.getErrorMessage(), e);
		//}
	}
	
	
	
}