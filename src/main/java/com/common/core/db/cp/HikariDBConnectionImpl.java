package com.common.core.db.cp;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.common.core.Config;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class HikariDBConnectionImpl implements DBConnection{

	private static final Logger logger = LoggerFactory.getLogger(HikariDBConnectionImpl.class);

	protected HikariDBConnectionImpl() {}
	
	private static HikariConfig config = new HikariConfig();
	private static HikariDataSource ds;
	    
	    
	@Override
	public void createDataSource(DBDetailsModel dbDetails, Properties props) {
		// TODO Auto-generated method stub
		//config.setJdbcUrl("jdbc:h2:mem:test");
        //config.setUsername("user");
        //config.setPassword("password");
        
		try {
			Class.forName(dbDetails.getDriverClass()).newInstance();
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return ;
		}
        
        config.setJdbcUrl(dbDetails.getConnectionURL());
        config.setUsername(dbDetails.getDbUsername());
        config.setPassword(dbDetails.getDbPassword());
        
        if (props.getProperty("cachePrepStmts") == null || "".equals(props.getProperty("cachePrepStmts")))
        	config.addDataSourceProperty("cachePrepStmts", "true");
        else 
        	config.addDataSourceProperty("cachePrepStmts", props.getProperty("cachePrepStmts"));
        
        
        if(props.getProperty("prepStmtCacheSize") == null || "".equals(props.getProperty("prepStmtCacheSize")))
        	config.addDataSourceProperty("prepStmtCacheSize", "250");
        else 
        	config.addDataSourceProperty("prepStmtCacheSize", props.getProperty("prepStmtCacheSize"));
        
        if (props.getProperty("prepStmtCacheSqlLimit") == null || "".equals(props.getProperty("prepStmtCacheSqlLimit")))
        	config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        else 
        	config.addDataSourceProperty("prepStmtCacheSqlLimit", props.getProperty("prepStmtCacheSqlLimit"));
        		
        
        
        
        ds = new HikariDataSource(config);
        
        if( null == props.getProperty("connectionTimeout") || "".equals(props.getProperty("connectionTimeout")))
        	 ds.setConnectionTimeout(600000);
        else
        	ds.setConnectionTimeout(Integer.parseInt(props.getProperty("connectionTimeout")));
        
        if (null == props.getProperty("maximumPoolSize") || "".equals(props.getProperty("maximumPoolSize")))
        	ds.setMaximumPoolSize(80);
        else 
        	ds.setMaximumPoolSize(Integer.parseInt(props.getProperty("maximumPoolSize")));
        
        
	}

	@Override
	public Connection getConnection() throws SQLException {
		// TODO Auto-generated method stub
		//try {
			return ds.getConnection();
		//} catch (SQLException e) {
		//	throw new SecException(SecErrorCode.SQL_EXCEPTION.getErrorCode(), SecErrorCode.SQL_EXCEPTION.getErrorMessage(), e);
		//}
	}
	
	
	

	    
	   

}
