package com.common.core.db.cp;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;


public interface DBConnection {
		
	void createDataSource(DBDetailsModel dbDetails, Properties props);
	public Connection getConnection() throws SQLException ;
	
}
