package com.common.core.db.cp;

import java.util.Properties;

import com.common.core.SecUtil;

public class DBCFactory {

	
	static DBConnection dsHikari;
	static DBConnection dsDbcp;
	
	public static DBConnection getDS(String type, Properties props) {
		
		
		DBDetailsModel dbDetails = SecUtil.getDbDetails();
		
		if(type == null || "".equals(type) || "Hikari".equalsIgnoreCase(type)) {
			if(dsHikari == null) {
				dsHikari = new HikariDBConnectionImpl(); 
				dsHikari.createDataSource(dbDetails, props);
			}
			
			return dsHikari;
		}
			
		else if ("dbcp".equalsIgnoreCase(type)) {
			if(dsDbcp == null) {
				dsDbcp = new DBCPDBConnectionImpl(); 
				dsDbcp.createDataSource(dbDetails, props);
			}
			return dsDbcp;
		}
			
		
		
		return null;
		
	}
}
