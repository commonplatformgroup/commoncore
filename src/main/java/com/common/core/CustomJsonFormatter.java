package com.common.core;

import java.io.IOException;
import java.util.Map;

import ch.qos.logback.contrib.jackson.JacksonJsonFormatter;

public class CustomJsonFormatter extends JacksonJsonFormatter {
	@Override
    public String toJsonString(Map map) throws IOException {
        map.put("service", "caas-vulcan");
        map.remove("thread");
        map.remove("context");
        map.remove("logger");
        return super.toJsonString(map);
    }
}
