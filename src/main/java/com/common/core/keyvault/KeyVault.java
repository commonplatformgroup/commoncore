package com.common.core.keyvault;

import com.common.core.errorhandling.SecException;

public abstract class KeyVault {

	
	public abstract void setKey(String key, String value) throws SecException;
	
	public abstract String getKey(String key) throws SecException;
}
