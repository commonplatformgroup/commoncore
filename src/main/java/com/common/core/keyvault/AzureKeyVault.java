package com.common.core.keyvault;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import com.common.core.errorhandling.SecException;
import com.common.core.logging.SecLogger;

import com.azure.core.util.polling.SyncPoller;
import com.azure.identity.DefaultAzureCredentialBuilder;

import com.azure.security.keyvault.secrets.SecretClient;
import com.azure.security.keyvault.secrets.SecretClientBuilder;
import com.azure.security.keyvault.secrets.models.DeletedSecret;
import com.azure.security.keyvault.secrets.models.KeyVaultSecret;

public class AzureKeyVault extends KeyVault {

	@Override
	public void setKey(String key, String value) throws SecException {
		String keyVaultName = System.getenv("KEY_VAULT_NAME");
		String keyVaultUri = "https://" + keyVaultName + ".vault.azure.net";

		SecretClient secretClient = new SecretClientBuilder()
		    .vaultUrl(keyVaultUri)
		    .credential(new DefaultAzureCredentialBuilder().build())
		    .buildClient();
		
		secretClient.setSecret(new KeyVaultSecret(key, value));
	}

	@Override
	public String getKey(String key) throws SecException {
		String keyVaultName = System.getenv("KEY_VAULT_NAME");
		String keyVaultUri = "https://" + keyVaultName + ".vault.azure.net";

		SecretClient secretClient = new SecretClientBuilder()
		    .vaultUrl(keyVaultUri)
		    .credential(new DefaultAzureCredentialBuilder().build())
		    .buildClient();
		KeyVaultSecret secret = secretClient.getSecret(key);
		return secret.getValue();
		
	}

	
}
