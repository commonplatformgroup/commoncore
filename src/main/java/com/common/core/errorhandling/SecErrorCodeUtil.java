package com.common.core.errorhandling;

import java.io.File;
import java.io.InputStream;
import java.util.Map;

import org.junit.platform.engine.support.descriptor.ClasspathResourceSource;
import com.common.core.Config;
import com.common.core.SecUtil;
import com.common.core.logging.SecLogger;

public class SecErrorCodeUtil {

	private static Config.Locale locale = Config.Locale.ENGLISH;
	private static InputStream erroCodeFileStream ;
	// private static Config.Locale locale;

	public static void setLocale(Config.Locale locale) {
		SecErrorCodeUtil.locale = locale;
	}

	public static void setErrorFile(InputStream in) {
		SecErrorCodeUtil.erroCodeFileStream = in;
	}
	private static Map<String, String> PROPS;

	public static Map<String, String> getProps() throws SecException {
		if (PROPS == null) {
			return refreshProps();
		}

		return PROPS;
	}

	public synchronized static Map<String, String> refreshProps() throws SecException {	
		
//		String filePath = SecUtil.getCommonHome("/Users/mohammedmujeeb/Downloads/NEXERHOME")
//				+ File.separator + "common_errorcodes_";
//		switch (locale) {
//
//		case ENGLISH:
//			filePath += "en.properties";
//			break;
//		case DUTCH:
//			filePath += "nl.properties";
//			break;
//		default:
//			filePath += "en.properties";
//			break;
//		}
	
		PROPS = SecUtil.readPropertiesFile(erroCodeFileStream);
		return PROPS;
	}


	
	public static SecErrorCode getSecErrorCode(String errorCode, String defaultErrorMessage) {

		try {
			if (null == getProps()) {
				return new SecErrorCode(errorCode, defaultErrorMessage);
			}
			if (getProps().get(errorCode) == null) {

				return new SecErrorCode(errorCode, defaultErrorMessage);
			}

			return new SecErrorCode(errorCode, getProps().get(errorCode));
		} catch (Exception e) {
			// SecLogger.error("Error code not found in properties. Code :" + errorCode
			// + " DefaultMessage : " + defaultErrorMessage, e);
		}

		return new SecErrorCode(errorCode, defaultErrorMessage);
	}

}
