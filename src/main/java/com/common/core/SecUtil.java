package com.common.core;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.common.core.db.cp.DBDetailsModel;
import com.common.core.errorhandling.SecErrorCode;
import com.common.core.errorhandling.SecException;
import com.common.core.logging.SecLogger;

/**
 * The Class SecUtil.
 */
public class SecUtil {

	private static final Logger logger = LoggerFactory.getLogger(SecUtil.class);
	private static String localKey;
	private static String dbType = "postgressql";

	private static final String ALGO = "AES";
	private static byte[] keyValue = new byte[] { 'S', '3', 'c', 'u', 'r', '1', '3', 'n', 'c', '3', '1', '2', '3', 'K',
			'e', 'y' };
	private static String propertiesFilePath = "";
	private static DBDetailsModel dbDetails = new DBDetailsModel();

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		System.out.println(getCommonHome());
	}

	/**
	 * Gets the Common home.
	 *
	 * @return the Common home
	 */
	public static String getCommonHome(String defaultPath) {

		SecLogger.info("COMMON_HOME => " + System.getenv("COMMON_HOME"));

		String secHome = System.getenv("COMMON_HOME");
		if (secHome != null) {
			return secHome;
		}
		SecLogger.warn("Common Home is not set. It is recommended to set the COMMON_HOME environment variable");
		return defaultPath;

	}

	public static String getCommonHome() {
		return getCommonHome(null);
	}

	/**
	 * Read properties file
	 *
	 * @return properties
	 */
	public static Map<String, String> readPropertiesFile(String path) throws SecException {

		Properties prop = new Properties();
		Map<String, String> response = new HashMap<String, String>();
		try {

			prop.load(new FileInputStream(path));
			for (final String name : prop.stringPropertyNames())
				response.put(name, prop.getProperty(name));

		} catch (IOException ex) {
			logger.error(ex.toString());
			throw new SecException(SecErrorCode.IO_EXCEPTION, ex);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new SecException(SecErrorCode.UNKNOWN_EXCEPTION, e);
		}
		return response;

	}
	
	/**
	 * Read properties file
	 *
	 * @return properties
	 */
	public static Map<String, String> readPropertiesFile(InputStream in) throws SecException {

		Properties prop = new Properties();
		Map<String, String> response = new HashMap<String, String>();
		try {

			prop.load(in);
			for (final String name : prop.stringPropertyNames())
				response.put(name, prop.getProperty(name));

		} catch (IOException ex) {
			logger.error(ex.toString());
			throw new SecException(SecErrorCode.IO_EXCEPTION, ex);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new SecException(SecErrorCode.UNKNOWN_EXCEPTION, e);
		}
		return response;

	}

//	public static void initConnectionDetails(){
//		
//		
//	    try {      
//	    		if (System.getenv("COMMON_HOME") != null) 
//				propertiesFilePath = System.getenv("COMMON_HOME") + "/conf/.properties";
//			else {
//				Properties localprop = new Properties();
//				InputStream in = AppUtil.class.getResourceAsStream("../package.properties");
//				localprop.load(in);
//				String SEC_HOME = localprop.getProperty("Commonhome");
//				in.close();
//				if(SEC_HOME != null) {
//					propertiesFilePath = SEC_HOME + "/conf/.properties";
//				} else {
//					propertiesFilePath = "/usr/local/.properties";
//				}
//			}
//	        AppConstants.PROPS.load(new FileInputStream(propertiesFilePath));
//	        String driverClass = AppConstants.PROPS.getProperty("driverClass");
//	        dbDetails.setDriverClass(driverClass);
//	        
//	        String username = AppConstants.PROPS.getProperty("username");
//	        dbDetails.setDbUsername(username);
//	        
//	        localKey = decrypt(AppConstants.PROPS.getProperty("EncryptionKey"),keyValue);
//	        
//	        
//	        String dbPassword = decrypt(AppConstants.PROPS.getProperty("DatabasePassword"),localKey.getBytes());
//	        dbDetails.setDbPassword(dbPassword);
//	        
//	        
//	        String connectionURL = AppConstants.PROPS.getProperty("connectionURL");
//	        dbDetails.setConnectionURL(connectionURL);
//	        
//	        dbType = AppConstants.PROPS.getProperty("database");
//	        dbDetails.setDbType(dbType);
//	        
//	        
//	    } catch (IOException ex) {
//	    	ErrorHandlingUtil.logError(logger, "There is an error while connecting to AppUtil for getting data from properties file, catched IOException ", ex);	    	
//	    	
//	    } catch (Exception e) {
//	    	ErrorHandlingUtil.logError(logger, "There is an error while connecting to AppUtil for getting data from properties file, catched Exception ", e);	    	
//	    	
//		}
//		
//	}

	public static DBDetailsModel getDbDetails() {
		return dbDetails;
	}

	/**
	 * Check that a string represents a decimal number
	 * 
	 * @param string The string to check
	 * @return true if string consists of only numbers without leading zeroes, false
	 *         otherwise
	 */
	public static boolean isDecimal(String string) {
		// Check whether string has a leading zero but is not "0"
		if (string.startsWith("0")) {
			return string.length() == 1;
		}
		for (char c : string.toCharArray()) {
			if (c < '0' || c > '9') {
				return false;
			}
		}
		return true;
	}

	public static boolean isIp(String string) {
		String[] parts = string.split("\\.", -1);
		return parts.length == 4 // 4 parts
				&& Arrays.stream(parts).filter(SecUtil::isDecimal) // Only decimal numbers
						.map(Integer::parseInt).filter(i -> i <= 255 && i >= 0) // Must be inside [0, 255]
						.count() == 4; // 4 numerical parts inside [0, 255]
	}

}
