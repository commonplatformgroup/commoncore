package com.common.core.throttle;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import com.common.core.logging.SecLogger;

public class IPThrottler extends Throttler {

	public boolean throttle(String ipAddress, String apiPath) {

		//
			String ipAndApi = ipAddress + apiPath;
			List<Calendar> hits = counter.get(ipAndApi);

			List<Calendar> updatedCount = new ArrayList<Calendar>();
			Calendar currentTime = Calendar.getInstance();
			if (hits != null) {
				Iterator<Calendar> itr = hits.iterator();

				while (itr.hasNext()) {
					Calendar item = itr.next();
					Calendar itemExpiryTime = (Calendar) item.clone();
					itemExpiryTime.add(Calendar.SECOND, timeLimtInSeconds);

					SecLogger.debug("IPThrottler content : " + item.toString());
					if (itemExpiryTime.after(currentTime)) {

						updatedCount.add(item);
					} else {

					}
				}
			}
			updatedCount.add(currentTime);

			
			synchronized (counter) {
				counter.put(ipAndApi, updatedCount);
			}
			if (updatedCount.size() > limit) {
				return true;
			}
	

		return false;
	}

	@Override
	public void setThrottleLimit(int limit, int timeLimitInSeconds) {
		this.limit = limit;
		this.timeLimtInSeconds = timeLimitInSeconds;
	}
}
