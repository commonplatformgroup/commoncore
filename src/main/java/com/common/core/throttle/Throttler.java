package com.common.core.throttle;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Throttler {

	Map<String, List<Calendar>> counter = new HashMap<String, List<Calendar>>();
	protected int limit = 0;
	protected int timeLimtInSeconds = 0;
	
	public abstract boolean throttle(String id, String apiPath);
	
	public abstract void setThrottleLimit(int limit, int timeLimitInSeconds);
}
